﻿using System;

namespace ex_18
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #18 - Assigning Variables
            // --------------------------------------------
            // In the slides of "bedmas - using a little maths" there was a slide with a number of calculations.
            // These calculations are in the google doc of this exercise.
            // Team up with someone or try them by yourself.
            // Note - for this exercise you only need to create a single repository.
            // All the calculations can be defined in the same Program.cs file.


            // Task A
            // a)  Create 5 variables and print to the screen the
            // name of the variable and then the value.
            // Example: “The value of num1 = 5”
            var num1 = 1;
            var num2 = 2;
            var num3 = 3;
            var num4 = 4;
            var num5 = 5;
            Console.WriteLine("Task A-------------------------------\n");
            Console.WriteLine($"The value of num1 = {num1}, num2 = {num2}, num3 = {num3}, num4 = {num4}, num5 = {num5}\n\n");

            // Task B
            // Create 6 variables and do the same as task a,
            // but create only 3 lines where you are adding
            // pairs of numbers together
            // Example:	“num1 + num2 = 34”
            var num6 = 6;
            Console.WriteLine("Task B-------------------------------\n");
            Console.WriteLine($"num1 + num2 = {num1 + num2}");
            Console.WriteLine($"num3 + num4 = {num3 + num4}");
            Console.WriteLine($"num5 + num6 = {num5 + num6}\n\n");


            // Task C
            // Create 4 variables and add them together, by
            // placing the answer in the original variable
            // Note: Do this with the shorthand code
            // Example the longhand version is: 
            // “num1 = num1 + num2”
            Console.WriteLine("Task C-------------------------------\n");
            Console.WriteLine($"The value of num1 = {num1}");
            num1 = num1 + num2;
            Console.WriteLine("num1 = num1 + num2");
            Console.WriteLine($"The value of num1 = {num1}\n");
            
            Console.WriteLine($"The value of num3 = {num3}");
            num3 = num3 + num4;
            Console.WriteLine("num3 = num3 + num4");
            Console.WriteLine($"The value of num3 = {num3}\n\n");

            // Task D
            // Create a variable with the words 
            // “I am thinking…”
            // Multiply that variable by 3
            // What happens when you compile it?
            /*
            var strTemp = "I am thinking...";
            var strXnumber = strTemp*3;
            Console.WriteLine($"What will be happened if I multiply string by 3 : {strXnumber}");
            */
        }
    }
}
